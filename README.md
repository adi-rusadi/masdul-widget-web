<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->
# Description

Masdul widget help in simplifying code for some complicated widget.

## Features

* Search Page

### Search Page

![Search Page](https://gitlab.com/adi-rusadi/masdul-widget-web/-/raw/main/screenshhots/searchpageweb.jpeg)

```dart
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:masdul_web/arguments/search_page_arguments.dart';
import 'package:masdul_web/models/search_page_widget_model.dart';
import 'package:masdul_web/search_page_widget.dart';

class SearchPageSample extends StatefulWidget {
  const SearchPageSample({super.key});

  @override
  State<SearchPageSample> createState() => _SearchPageSampleState();
}

class _SearchPageSampleState extends State<SearchPageSample> {
  var faker = Faker();
  late List<SearchPageWidgetModel<SearchPageSampleModel>> sampleData;
  late List<SearchPageWidgetModel<SearchPageSampleModel>> searchData;
  late List<SearchPageWidgetModel<SearchPageSampleModel>> searchDataModal;

  @override
  void initState() {
    sampleData = [];
    for (var i = 0; i < 50; i++) {
      var faker = Faker();
      var sData = SearchPageSampleModel(
        id: faker.guid.random.toString(),
        address: faker.address.city(),
        age: 23,
        name: faker.person.name(),
        salary: faker.randomGenerator.integer(1000000, min: 50000),
      );
      sampleData.add(SearchPageWidgetModel(
        title: sData.name,
        description: sData.address,
        value: sData,
      ));
      searchData = sampleData;
      searchDataModal = sampleData;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Search Page"),
        actions: [
          TextButton(
              onPressed: () {
                searchDataModal = searchData;
                SearchPageWidget.modal<SearchPageSampleModel>(
                  context,
                  args: SearchPageArguments<SearchPageSampleModel>(
                    data: searchDataModal,
                    color: Colors.blue,
                    hint: "Search ...",
                    title: "Search Employee",
                    // isLocalSearch: true,
                    onTextChanged: (text, localSearch) async {
                      searchDataModal = sampleData
                          .where(
                            (element) => element.title
                                .toLowerCase()
                                .contains(text.trim().toLowerCase()),
                          )
                          .toList();
                      return searchDataModal;
                      // setState(() {});
                    },
                  ),
                );
              },
              child: const Text(
                "Modal",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
        child: SearchPageWidget<SearchPageSampleModel>(
          args: SearchPageArguments<SearchPageSampleModel>(
            data: searchData,
            color: Colors.blue,
            hint: "Search ...",
            title: "Search Employee",
            // isLocalSearch: true,
            onTextChanged: (text, localSearch) async {
              searchData = sampleData
                  .where(
                    (element) => element.title
                        .toLowerCase()
                        .contains(text.trim().toLowerCase()),
                  )
                  .toList();
              return searchData;
              // setState(() {});
            },
          ),
        ),
      ),
    );
  }
}

class SearchPageSampleModel {
  final String id;
  final String name;
  final String address;
  final int age;
  final int salary;

  SearchPageSampleModel({
    required this.id,
    required this.name,
    required this.address,
    required this.age,
    required this.salary,
  });
}

```

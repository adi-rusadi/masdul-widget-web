import 'package:flutter/material.dart';

class Layout extends StatelessWidget {
  final bool isLoading;
  final Widget child;
  const Layout({super.key, required this.isLoading, required this.child});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: child,
        ),
        if (isLoading)
          Container(
            color: Colors.black.withOpacity(0.05),
            child: const CircularProgressIndicator(),
          )
      ],
    );
  }
}
